# Use an official Maven image with JDK 17 to build the project
FROM maven:3.8.4-openjdk-17 as builder
WORKDIR /app

# Tận dụng lợi thế của Docker layer caching
COPY pom.xml .
RUN mvn dependency:go-offline

COPY src ./src
RUN mvn package -DskipTests

# Use an official OpenJDK 17 image to run the compiled JAR
FROM openjdk:17-jdk-slim
WORKDIR /app
COPY --from=builder /app/target/*.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","app.jar"]

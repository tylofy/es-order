package com.es.order;
import static net.bytebuddy.matcher.ElementMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.mockito.Mockito.*;

import com.es.order.controller.OrderController;
import com.es.order.entity.OrderDetail;
import com.es.order.model.CreateOrderRequest;
import com.es.order.model.GetOrderResponse;
import com.es.order.model.GetOrderSummaryResponse;
import com.es.order.model.UpdateOrderRequest;
import com.es.order.service.OrderService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.test.web.servlet.ResultMatcher;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@WebMvcTest(OrderController.class)
public class OrderControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OrderService orderService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void createOrder_ShouldReturnOrderDetail() throws Exception {
        CreateOrderRequest request = new CreateOrderRequest();
        request.setDescription("Sample description");
        request.setNotes("Sample notes");
        request.setQuantity(10L);
        request.setCategoryId("cat123");
        request.setServiceNameId("service123");
        request.setUserId("user123");

        OrderDetail orderDetail = new OrderDetail();
        orderDetail.setId("order123");
        orderDetail.setReference("ref123");
        orderDetail.setQuantity(request.getQuantity());
        orderDetail.setDescription(request.getDescription());
        orderDetail.setNotes(request.getNotes());
        orderDetail.setTotalAmount(100.0);
        // Assume Category, ServiceName, and User are set up appropriately

        when(orderService.createOrder(any(CreateOrderRequest.class))).thenReturn(orderDetail);

        mockMvc.perform(post("/api/order")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.id").value(orderDetail.getId()))
                .andExpect(jsonPath("$.data.reference").value(orderDetail.getReference()))
                .andExpect(jsonPath("$.data.quantity").value(orderDetail.getQuantity()))
                .andExpect(jsonPath("$.data.description").value(orderDetail.getDescription()))
                .andExpect(jsonPath("$.data.notes").value(orderDetail.getNotes()))
                .andExpect(jsonPath("$.data.totalAmount").value(orderDetail.getTotalAmount()));

        verify(orderService, times(1)).createOrder(any(CreateOrderRequest.class));
    }

    @Test
    public void updateOrderByUser_ShouldReturnUpdatedOrderDetail() throws Exception {
        String orderId = "order123";
        String userId = "user123";
        UpdateOrderRequest request = new UpdateOrderRequest();
        request.setDescription("Sample description");
        request.setNotes("Sample notes");
        request.setQuantity(10L);
        request.setCategoryId("cat123");
        request.setServiceNameId("service123");

        OrderDetail updatedOrderDetail = new OrderDetail();
        updatedOrderDetail.setReference("ref123");
        updatedOrderDetail.setQuantity(request.getQuantity());
        updatedOrderDetail.setDescription(request.getDescription());
        updatedOrderDetail.setNotes(request.getNotes());
        updatedOrderDetail.setTotalAmount(100.0);


        when(orderService.updateOrderByUser(eq(orderId), eq(userId), any(UpdateOrderRequest.class)))
                .thenReturn(updatedOrderDetail);

        mockMvc.perform(put("/api/order/{id}", orderId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
                        .param("userId", userId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data").exists());  // Customize based on your response structure

        verify(orderService, times(1)).updateOrderByUser(eq(orderId), eq(userId), any(UpdateOrderRequest.class));
    }

    @Test
    public void deleteOrderByUser_ShouldReturnSuccessMessage() throws Exception {
        String orderId = "order123";
        String userId = "user123";
        String successMessage = "deleted: " + orderId;

        when(orderService.deleteOrderByUser(eq(orderId), eq(userId))).thenReturn(successMessage);

        mockMvc.perform(delete("/api/order/{id}", orderId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("userId", userId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data").value(successMessage));

        verify(orderService, times(1)).deleteOrderByUser(eq(orderId), eq(userId));
    }

    @Test
    public void getAllOrderByUser_ShouldReturnOrders() throws Exception {
        String userId = "user123";
        GetOrderResponse orderResponse1 = createTestOrderResponse("1", "Description 1", "Note 1", 5L, "REF123", 100.0, "Electronics", "Warranty Service", userId, "John Doe", LocalDateTime.now());
        GetOrderResponse orderResponse2 = createTestOrderResponse("2", "Description 2", "Note 2", 10L, "REF456", 200.0, "Books", "Delivery Service", userId, "John Doe", LocalDateTime.now());

        List<GetOrderResponse> orderResponses = Arrays.asList(orderResponse1, orderResponse2);

        when(orderService.getAllOrderByUser(eq(userId))).thenReturn(orderResponses);

        mockMvc.perform(get("/api/order")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("userId", userId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data", hasSize(2)));
        verify(orderService, times(1)).getAllOrderByUser(eq(userId));
    }

    private GetOrderResponse createTestOrderResponse(String id, String description, String notes, Long quantity, String reference, Double totalAmount, String categoryName, String serviceName, String userId, String username, LocalDateTime createdAt) {
        GetOrderResponse response = new GetOrderResponse();
        response.setId(id);
        response.setDescription(description);
        response.setNotes(notes);
        response.setQuantity(quantity);
        response.setReference(reference);
        response.setTotalAmount(totalAmount);
        response.setCategoryName(categoryName);
        response.setServiceName(serviceName);
        response.setUserId(userId);
        response.setUsername(username);
        response.setCreatedAt(createdAt);
        return response;
    }

    @Test
    public void countOrderByUser_ShouldReturnOrderCount() throws Exception {
        String userId = "user123";
        Long expectedCount = 5L;

        when(orderService.countOrderByUser(userId)).thenReturn(expectedCount);

        mockMvc.perform(get("/api/order/order-count", userId)
                        .param("userId", userId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data").value(expectedCount));

        verify(orderService, times(1)).countOrderByUser(userId);
    }

    @Test
    public void revenueByUser_ShouldReturnRevenue() throws Exception {
        String userId = "user123";
        Double expectedRevenue = 5000.0;

        when(orderService.revenueByUser(userId)).thenReturn(expectedRevenue);

        mockMvc.perform(get("/api/order/revenue", userId)
                        .param("userId", userId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data").value(expectedRevenue));

        verify(orderService, times(1)).revenueByUser(userId);
    }

    @Test
    public void getOrderSummary_ShouldReturnSummary() throws Exception {
        LocalDate startDate = LocalDate.of(2024, 1, 1);
        LocalDate endDate = LocalDate.of(2024, 1, 31);
        GetOrderSummaryResponse summaryResponse = new GetOrderSummaryResponse();
        summaryResponse.setCount(10L);
        summaryResponse.setRevenue(10000.0);

        when(orderService.getOrdersSummary(startDate, endDate)).thenReturn(summaryResponse);

        mockMvc.perform(get("/api/order/summary")
                        .param("startDate", startDate.toString())
                        .param("endDate", endDate.toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.count").value(summaryResponse.getCount()))
                .andExpect(jsonPath("$.data.revenue").value(summaryResponse.getRevenue()));

        verify(orderService, times(1)).getOrdersSummary(startDate, endDate);
    }
}


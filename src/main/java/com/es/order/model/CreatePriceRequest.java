package com.es.order.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreatePriceRequest {
    private String categoryId;
    private String serviceNameId;
    private Double price;
}

package com.es.order.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetOrderResponse {
    private String id;
    private String description;
    private String notes;
    private Long quantity;
    private String reference;
    private Double totalAmount;
    private String categoryName;
    private String serviceName;
    private String userId;
    private String username;
    private LocalDateTime createdAt;
}

package com.es.order.model;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateOrderRequest {
    private String description;
    private String notes;
    @NotNull
    private Long quantity;
    @NotNull
    private String categoryId;
    @NotNull
    private String serviceNameId;
    @NotNull
    private String userId;
}

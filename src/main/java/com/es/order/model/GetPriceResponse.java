package com.es.order.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetPriceResponse {
    private String id;
    private String categoryId;
    private String categoryName;
    private String serviceNameId;
    private String serviceName;
    private Double price;
}

package com.es.order.repository;

import com.es.order.entity.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface OrderRepository extends JpaRepository<OrderDetail, String> {
    List<OrderDetail> findByUserId(String userId);

    Long countByUserId(String userId);

    Long countByCreatedAtBetween(LocalDateTime startDate, LocalDateTime endDate);

    @Query("SELECT SUM(o.totalAmount) FROM OrderDetail o WHERE o.createdAt BETWEEN :startDate AND :endDate")
    Double sumOrderAmountsBetweenDates(LocalDateTime startDate, LocalDateTime endDate);
}

package com.es.order.repository;

import com.es.order.entity.ServiceName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ServiceNameRepository extends JpaRepository<ServiceName, String> {
    Optional<ServiceName> findByName(String name);
}

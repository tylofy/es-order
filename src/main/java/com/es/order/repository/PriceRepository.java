package com.es.order.repository;

import com.es.order.entity.Category;
import com.es.order.entity.CategoryServiceName;
import com.es.order.entity.ServiceName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PriceRepository extends JpaRepository<CategoryServiceName, String> {

    Optional<CategoryServiceName> findByCategoryAndServiceName(Category category, ServiceName serviceName);
}

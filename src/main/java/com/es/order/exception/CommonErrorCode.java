package com.es.order.exception;

import org.springframework.http.HttpStatus;

public enum CommonErrorCode implements AbstractError {

    BAD_REQUEST(1,
            "Bad request",
            HttpStatus.BAD_REQUEST),
    FORBIDDEN(2,
            "You do not have permission to perform this operation.",
            HttpStatus.FORBIDDEN),
    NOT_FOUND(3, "Not found", HttpStatus.NOT_FOUND),
    METHOD_NOT_ALLOWED(4,
            "Request method is known by the server but is not supported by the target resource",
            HttpStatus.METHOD_NOT_ALLOWED),
    DATA_INTEGRITY_VIOLATION(5,
            "An attempt to insert or update data results in violation of an integrity constraint.",
            HttpStatus.CONFLICT),
    MISSING_REQUEST_PARAMETER(6,
            "Required parameter is not present",
            HttpStatus.BAD_REQUEST),
    UNSUPPORTED_MEDIA_TYPE(7,
            "The server refuses to accept the request because the payload format is in an unsupported format",
            HttpStatus.UNSUPPORTED_MEDIA_TYPE),
    MEDIA_TYPE_NOT_ACCEPTABLE(8,
            "The request handler cannot generate a response that is acceptable by the client",
            HttpStatus.UNSUPPORTED_MEDIA_TYPE),
    ARGUMENT_TYPE_MISMATCH(9,
            "Required type parameter is not match",
            HttpStatus.BAD_REQUEST),
    ARGUMENT_NOT_VALID(10, "Validation failed", HttpStatus.BAD_REQUEST),
    ENTITY_NOT_FOUND(11, "Object no longer exists in the database",
            HttpStatus.NOT_FOUND),
    INTERNAL_SERVER_ERROR(12,
            "There was an error processing the request, please contact the administrator!",
            HttpStatus.INTERNAL_SERVER_ERROR),
    USER_NOT_FOUND(13, "User not found", HttpStatus.UNAUTHORIZED),
    CATEGORY_NOT_FOUND(14, "Category not found", HttpStatus.NOT_FOUND),
    SERVICE_NAME_NOT_FOUND(15, "Service name not found", HttpStatus.NOT_FOUND),
    ORDER_DETAIL_NOT_FOUND(16, "Order detail not found", HttpStatus.NOT_FOUND),
    PRICE_NOT_FOUND(17, "Price not found", HttpStatus.NOT_FOUND);

    private final int code;

    private final String message;

    private final HttpStatus httpStatus;

    CommonErrorCode(int code, String message, HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
        this.code = code;
        this.message = message;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}

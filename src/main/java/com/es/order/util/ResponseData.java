package com.es.order.util;

import lombok.Getter;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
public class ResponseData<T> implements Serializable {

  private static final long serialVersionUID = 1L;

  private final LocalDateTime timestamp;

  private boolean isEncrypt;

  private int code;

  private String msg;

  private T data;

  ResponseData(boolean isEncrypt) {
    this.code = 0;
    this.timestamp = LocalDateTime.now();
    this.msg = "Successful!";
    this.isEncrypt = isEncrypt;
  }

  ResponseData<T> success(T data) {
    this.data = data;
    return this;
  }

  ResponseData<T> error(int code, String message) {
    this.code = code;
    this.msg = message;
    return this;
  }

  ResponseData<T> error(int code, String message, T data) {
    this.data = data;
    this.code = code;
    this.msg = message;
    return this;
  }

  public void setData(T data) {
    this.data = data;
  }

  public void setEncrypt(boolean encrypt) {
    isEncrypt = encrypt;
  }
}

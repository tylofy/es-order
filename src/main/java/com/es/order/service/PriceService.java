package com.es.order.service;

import com.es.order.entity.Category;
import com.es.order.entity.CategoryServiceName;
import com.es.order.entity.ServiceName;
import com.es.order.exception.BaseException;
import com.es.order.exception.CommonErrorCode;
import com.es.order.model.CreatePriceRequest;
import com.es.order.model.GetPriceResponse;
import com.es.order.repository.CategoryRepository;
import com.es.order.repository.PriceRepository;
import com.es.order.repository.ServiceNameRepository;
import jakarta.transaction.Transactional;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PriceService {
    @Autowired
    private final CategoryRepository categoryRepository;
    @Autowired
    private final ServiceNameRepository serviceNameRepository;
    @Autowired
    private final PriceRepository priceRepository;

    public PriceService(CategoryRepository categoryRepository, ServiceNameRepository serviceNameRepository, PriceRepository priceRepository) {
        this.categoryRepository = categoryRepository;
        this.serviceNameRepository = serviceNameRepository;
        this.priceRepository = priceRepository;
    }

    @Transactional
    public CategoryServiceName createOrUpdatePrice(@NotNull CreatePriceRequest request) {
        Category category = categoryRepository.findById(request.getCategoryId())
                .orElseThrow(() -> new BaseException(CommonErrorCode.CATEGORY_NOT_FOUND));

        ServiceName serviceName = serviceNameRepository.findById(request.getServiceNameId())
                .orElseThrow(() -> new BaseException(CommonErrorCode.SERVICE_NAME_NOT_FOUND));

        // Kiểm tra xem một bản ghi tương tự đã tồn tại chưa
        Optional<CategoryServiceName> existingRecord = priceRepository
                .findByCategoryAndServiceName(category, serviceName);

        if (existingRecord.isPresent()) {
            // Cập nhật giá nếu bản ghi đã tồn tại
            CategoryServiceName updatedRecord = existingRecord.get();
            updatedRecord.setPrice(request.getPrice());
            return priceRepository.save(updatedRecord);
        } else {
            // Tạo một bản ghi mới nếu không tìm thấy bản ghi tương tự
            CategoryServiceName newRecord = new CategoryServiceName();
            newRecord.setCategory(category);
            newRecord.setServiceName(serviceName);
            newRecord.setPrice(request.getPrice());
            return priceRepository.save(newRecord);
        }

    }

    public GetPriceResponse getPrice(String id) {
        CategoryServiceName price = priceRepository.findById(id)
                .orElseThrow(() -> new BaseException(CommonErrorCode.PRICE_NOT_FOUND));
        GetPriceResponse getCategoryResponse = GetPriceResponse.builder()
                .id(id)
                .categoryId(price.getCategory().getId())
                .categoryName(price.getCategory().getName())
                .serviceNameId(price.getServiceName().getId())
                .serviceName(price.getServiceName().getName())
                .price(price.getPrice())
                .build();
        return getCategoryResponse;
    }

    public List<GetPriceResponse> getAllPrice() {
        List<CategoryServiceName> prices = priceRepository.findAll();
        return prices.stream()
                .map(price -> new GetPriceResponse(price.getId(),price.getCategory().getId(), price.getCategory().getName(),
                        price.getServiceName().getId(), price.getServiceName().getName(),price.getPrice()))
                .collect(Collectors.toList());
    }

    @Transactional
    public String deletePrice(String id) {
        CategoryServiceName price = priceRepository.findById(id)
                .orElseThrow(() -> new BaseException(CommonErrorCode.PRICE_NOT_FOUND));
        this.priceRepository.delete(price);
        return "deleted: " + id;

    }
}

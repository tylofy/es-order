package com.es.order.service;

import com.es.order.entity.*;
import com.es.order.exception.BaseException;
import com.es.order.exception.CommonErrorCode;
import com.es.order.model.CreateOrderRequest;
import com.es.order.model.GetOrderResponse;
import com.es.order.model.GetOrderSummaryResponse;
import com.es.order.model.UpdateOrderRequest;
import com.es.order.repository.*;
import jakarta.transaction.Transactional;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class OrderService {
    @Autowired
    private final OrderRepository orderRepository;
    @Autowired
    private final CategoryRepository categoryRepository;
    @Autowired
    private final ServiceNameRepository serviceNameRepository;
    @Autowired
    private final UserRepository userRepository;
    @Autowired
    private final PriceRepository priceRepository;

    public OrderService(OrderRepository orderRepository, CategoryRepository categoryRepository, ServiceNameRepository serviceNameRepository, UserRepository userRepository, PriceRepository priceRepository) {
        this.orderRepository = orderRepository;
        this.categoryRepository = categoryRepository;
        this.serviceNameRepository = serviceNameRepository;
        this.userRepository = userRepository;
        this.priceRepository = priceRepository;
    }

    @Transactional
    public OrderDetail createOrder(@NotNull CreateOrderRequest request) {
        Category category = categoryRepository.findById(request.getCategoryId())
                .orElseThrow(() -> new BaseException(CommonErrorCode.CATEGORY_NOT_FOUND));
        ServiceName serviceName = serviceNameRepository.findById(request.getServiceNameId())
                .orElseThrow(() -> new BaseException(CommonErrorCode.SERVICE_NAME_NOT_FOUND));
        User user = userRepository.findById(request.getUserId())
                .orElseThrow(() -> new BaseException(CommonErrorCode.USER_NOT_FOUND));
        String reference = getReference(request.getQuantity(), category.getName(), serviceName.getName(), user.getUsername());
        //get total_amount
        //get price from category_service_name table
        CategoryServiceName price = priceRepository.findByCategoryAndServiceName(category, serviceName)
                .orElseThrow(() -> new BaseException(CommonErrorCode.PRICE_NOT_FOUND));
        double total_amount = request.getQuantity() * price.getPrice();

        //save orderDetail
        OrderDetail orderDetail = OrderDetail.builder()
                .createdAt(LocalDateTime.now())
                .description(request.getDescription())
                .notes(request.getNotes())
                .quantity(request.getQuantity())
                .reference(reference)
                .totalAmount(total_amount)
                .category(category)
                .serviceName(serviceName)
                .user(user)
                .build();
        return orderRepository.save(orderDetail);
    }

    //Generate unique reference
    //Reference = Quantity_CATEGORY_SERVICE_Username_UUID
    @NotNull
    private static String getReference(@NotNull Long quantity, String categoryName, String serviceName, String username) {
        String reference = quantity + "_"
                + categoryName + "_"
                + serviceName + "_"
                + username + "_"
                + UUID.randomUUID();
        return reference;
    }

    @Transactional
    public OrderDetail updateOrderByUser(String id, String userId, @NotNull UpdateOrderRequest request) {
        OrderDetail orderDetail = orderRepository.findById(id)
                .orElseThrow(() -> new BaseException(CommonErrorCode.ORDER_DETAIL_NOT_FOUND));
        Category category = categoryRepository.findById(request.getCategoryId())
                .orElseThrow(() -> new BaseException(CommonErrorCode.CATEGORY_NOT_FOUND));
        ServiceName serviceName = serviceNameRepository.findById(request.getServiceNameId())
                .orElseThrow(() -> new BaseException(CommonErrorCode.SERVICE_NAME_NOT_FOUND));
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new BaseException(CommonErrorCode.USER_NOT_FOUND));
        // Check if the order belongs to the user
        if (!orderDetail.getUser().getId().equals(userId)){
            throw new BaseException(CommonErrorCode.FORBIDDEN);
        }
        String reference = getReference(request.getQuantity(), category.getName(), serviceName.getName(), user.getUsername());

        //get total_amount
        //get price from category_service_name table
        CategoryServiceName price = priceRepository.findByCategoryAndServiceName(category, serviceName)
                .orElseThrow(() -> new BaseException(CommonErrorCode.PRICE_NOT_FOUND));
        double total_amount = request.getQuantity() * price.getPrice();

        //update orderDetail
        orderDetail.setCreatedAt(LocalDateTime.now());
        if (request.getDescription() != null) {
            orderDetail.setDescription(request.getDescription());
        }
        if (request.getNotes() != null) {
            orderDetail.setNotes(request.getNotes());
        }
        if (request.getQuantity() != null) {
            orderDetail.setQuantity(request.getQuantity());
        }
        orderDetail.setReference(reference);
        orderDetail.setTotalAmount(total_amount);
        orderDetail.setCategory(category);
        orderDetail.setServiceName(serviceName);
        orderDetail.setUser(user);
        return orderRepository.save(orderDetail);
    }

    @Transactional
    public String deleteOrderByUser(String id, String userId) {
        OrderDetail orderDetail = orderRepository.findById(id)
                .orElseThrow(() -> new BaseException(CommonErrorCode.ORDER_DETAIL_NOT_FOUND));
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new BaseException(CommonErrorCode.USER_NOT_FOUND));
        // Check if the order not belongs to the user
        if (!orderDetail.getUser().getId().equals(userId)){
            throw new BaseException(CommonErrorCode.FORBIDDEN);
        }
        orderRepository.delete(orderDetail);
        return "deleted: " + id;
    }

    public List<GetOrderResponse> getAllOrderByUser(String userId) {
        List<OrderDetail> orderDetails = orderRepository.findByUserId(userId);
        return orderDetails.stream()
                .map(orderDetail -> {
                    GetOrderResponse response = new GetOrderResponse();
                    response.setId(orderDetail.getId());
                    response.setDescription(orderDetail.getDescription());
                    response.setNotes(orderDetail.getNotes());
                    response.setQuantity(orderDetail.getQuantity());
                    response.setReference(orderDetail.getReference());
                    response.setTotalAmount(orderDetail.getTotalAmount());
                    response.setCategoryName(orderDetail.getCategory().getName());
                    response.setServiceName(orderDetail.getServiceName().getName());
                    response.setUserId(orderDetail.getUser().getId());
                    response.setUsername(orderDetail.getUser().getUsername());
                    response.setCreatedAt(orderDetail.getCreatedAt());
                    return response;
                })
                .collect(Collectors.toList());
    }

    public Long countOrderByUser(String userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new BaseException(CommonErrorCode.USER_NOT_FOUND));
        return orderRepository.countByUserId(userId);
    }

    public Double revenueByUser(String userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new BaseException(CommonErrorCode.USER_NOT_FOUND));
        List<OrderDetail> orderDetails = orderRepository.findByUserId(userId);
        double revenue = orderDetails.stream()
                .mapToDouble(OrderDetail::getTotalAmount)
                .sum();
        return revenue;
    }

    public GetOrderSummaryResponse getOrdersSummary(LocalDate  startDate, LocalDate endDate) {
        LocalDateTime startDateTime = startDate.atStartOfDay();
        LocalDateTime endDateTime = endDate.atTime(23, 59, 59);
        long numberOfOrders = orderRepository.countByCreatedAtBetween(startDateTime, endDateTime);
        Double totalRevenue = orderRepository.sumOrderAmountsBetweenDates(startDateTime, endDateTime);
        GetOrderSummaryResponse getOrderSummaryResponse = GetOrderSummaryResponse.builder()
                .count(numberOfOrders)
                .revenue(totalRevenue)
                .build();
        return getOrderSummaryResponse;
    }
}

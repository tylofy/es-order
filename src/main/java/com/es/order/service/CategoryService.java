package com.es.order.service;

import com.es.order.entity.Category;
import com.es.order.exception.BaseException;
import com.es.order.exception.CommonErrorCode;
import com.es.order.model.CreateCategoryRequest;
import com.es.order.model.GetCategoryResponse;
import com.es.order.repository.CategoryRepository;
import jakarta.transaction.Transactional;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CategoryService {
    @Autowired
    private final CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Transactional
    public Category createCategory(@NotNull CreateCategoryRequest request) {
        Optional<Category> categoryOptional = categoryRepository.findByName(request.getName());
        if (categoryOptional.isPresent()) {
            throw new BaseException(CommonErrorCode.BAD_REQUEST);
        }
        Category category = Category.builder()
                .name(request.getName())
                .build();
        return categoryRepository.save(category);

    }

    public GetCategoryResponse getCategory(String id) {
        Optional<Category> categoryOptional = categoryRepository.findById(id);
        if (categoryOptional.isEmpty()) {
            throw new BaseException(CommonErrorCode.CATEGORY_NOT_FOUND);
        }
        GetCategoryResponse getCategoryResponse = GetCategoryResponse.builder()
                .name(categoryOptional.get().getName())
                .build();
        return getCategoryResponse;
    }

    public List<GetCategoryResponse> getAllCategory() {
        List<Category> categories = categoryRepository.findAll();
        return categories.stream()
                .map(category -> new GetCategoryResponse(category.getId(), category.getName()))
                .collect(Collectors.toList());
    }

    @Transactional
    public Category updateCategory(String id, @NotNull CreateCategoryRequest request) {
        Category categoryUpdate = categoryRepository.findById(id)
                .orElseThrow(() -> new BaseException(CommonErrorCode.CATEGORY_NOT_FOUND));
        categoryUpdate.setName(request.getName());
        return categoryRepository.save(categoryUpdate);
    }

    @Transactional
    public String deleteCategory(String id) {
        Category category = categoryRepository.findById(id)
                .orElseThrow(() -> new BaseException(CommonErrorCode.CATEGORY_NOT_FOUND));
        this.categoryRepository.delete(category);
        return "deleted: " + id;

    }
}

package com.es.order.service;

import com.es.order.entity.ServiceName;
import com.es.order.exception.BaseException;
import com.es.order.exception.CommonErrorCode;
import com.es.order.model.CreateServiceNameRequest;
import com.es.order.model.GetServiceNameResponse;
import com.es.order.repository.ServiceNameRepository;
import jakarta.transaction.Transactional;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ServiceNameService {
    @Autowired
    private final ServiceNameRepository serviceNameRepository;

    public ServiceNameService(ServiceNameRepository serviceNameRepository) {
        this.serviceNameRepository = serviceNameRepository;
    }

    @Transactional
    public ServiceName createServiceName(@NotNull CreateServiceNameRequest request) {
        Optional<ServiceName> ServiceNameOptional = serviceNameRepository.findByName(request.getName());
        if (ServiceNameOptional.isPresent()) {
            throw new BaseException(CommonErrorCode.BAD_REQUEST);
        }
        ServiceName serviceName = ServiceName.builder()
                .name(request.getName())
                .build();
        return serviceNameRepository.save(serviceName);

    }

    public GetServiceNameResponse getServiceName(String id) {
        Optional<ServiceName> serviceNameOptional = serviceNameRepository.findById(id);
        if (serviceNameOptional.isEmpty()) {
            throw new BaseException(CommonErrorCode.SERVICE_NAME_NOT_FOUND);
        }
        GetServiceNameResponse getServiceNameResponse = GetServiceNameResponse.builder()
                .name(serviceNameOptional.get().getName())
                .build();
        return getServiceNameResponse;
    }

    public List<GetServiceNameResponse> getAllServiceName() {
        List<ServiceName> serviceNames = serviceNameRepository.findAll();
        return serviceNames.stream()
                .map(ServiceName -> new GetServiceNameResponse(ServiceName.getId(), ServiceName.getName()))
                .collect(Collectors.toList());
    }

    @Transactional
    public ServiceName updateServiceName(String id, @NotNull CreateServiceNameRequest request) {
        ServiceName serviceNameUpdate = serviceNameRepository.findById(id)
                .orElseThrow(() -> new BaseException(CommonErrorCode.SERVICE_NAME_NOT_FOUND));
        serviceNameUpdate.setName(request.getName());
        return serviceNameRepository.save(serviceNameUpdate);
    }

    @Transactional
    public String deleteServiceName(String id) {
        ServiceName serviceName = serviceNameRepository.findById(id)
                .orElseThrow(() -> new BaseException(CommonErrorCode.SERVICE_NAME_NOT_FOUND));
        this.serviceNameRepository.delete(serviceName);
        return "deleted: " + id;

    }
}

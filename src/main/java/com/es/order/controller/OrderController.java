package com.es.order.controller;

import com.es.order.entity.OrderDetail;
import com.es.order.exception.BaseException;
import com.es.order.model.CreateOrderRequest;
import com.es.order.model.GetOrderResponse;
import com.es.order.model.GetOrderSummaryResponse;
import com.es.order.model.UpdateOrderRequest;
import com.es.order.service.OrderService;
import com.es.order.util.ResponseData;
import com.es.order.util.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/order")
public class OrderController {
    @Autowired
    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping
    public ResponseEntity<ResponseData<OrderDetail>> createOrder(@RequestBody CreateOrderRequest request) throws BaseException {
        return ResponseUtils.success(orderService.createOrder(request));
    }

    @PutMapping("/{id}")
    public ResponseEntity<ResponseData<OrderDetail>> updateOrderByUser(@PathVariable(name = "id") String id,
                                                                 @RequestBody UpdateOrderRequest request,
                                                                 String userId) throws BaseException {
        return ResponseUtils.success(orderService.updateOrderByUser(id, userId, request));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseData<String>> deleteOrderByUser(@PathVariable(name = "id") String id,
                                                            String userId) throws BaseException {
        return ResponseUtils.success(orderService.deleteOrderByUser(id, userId));
    }

    @GetMapping
    public ResponseEntity<ResponseData<List<GetOrderResponse>>> getAllOrderByUser(@RequestParam String userId) throws BaseException {
        return ResponseUtils.success(orderService.getAllOrderByUser(userId));
    }

    @GetMapping("/order-count")
    public ResponseEntity<ResponseData<Long>> countOrderByUser(@RequestParam String userId) {
        return ResponseUtils.success(orderService.countOrderByUser(userId));
    }

    @GetMapping("/revenue")
    public ResponseEntity<ResponseData<Double>> revenueByUser(@RequestParam String userId) {
        return ResponseUtils.success(orderService.revenueByUser(userId));
    }

    @GetMapping("/summary")
    public ResponseEntity<ResponseData<GetOrderSummaryResponse>> getOrderSummary(
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDate,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate  endDate) {
        return ResponseUtils.success(orderService.getOrdersSummary(startDate, endDate));
    }

}

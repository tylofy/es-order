package com.es.order.controller;

import com.es.order.entity.Category;
import com.es.order.exception.BaseException;
import com.es.order.model.CreateCategoryRequest;
import com.es.order.model.GetCategoryResponse;
import com.es.order.service.CategoryService;
import com.es.order.util.ResponseData;
import com.es.order.util.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/category")
public class CategoryController {
    @Autowired
    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @PostMapping
    public ResponseEntity<ResponseData<Category>> createCategory(@RequestBody CreateCategoryRequest request) throws BaseException {
        Category createdCategory = categoryService.createCategory(request);
        return ResponseUtils.success(createdCategory);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseData<GetCategoryResponse>> getCategory(@PathVariable(name = "id") String id) throws BaseException {
        return ResponseUtils.success(categoryService.getCategory(id));
    }

    @GetMapping
    public ResponseEntity<ResponseData<List<GetCategoryResponse>>> getAllCategory() throws BaseException {
        return ResponseUtils.success(categoryService.getAllCategory());
    }

    @PutMapping("/{id}")
    public ResponseEntity<ResponseData<Category>> updateCategory(@PathVariable(name = "id") String id,
                                                                 @RequestBody CreateCategoryRequest request) throws BaseException {
        return ResponseUtils.success(categoryService.updateCategory(id, request));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseData<String>> deleteCategory(@PathVariable(name = "id") String id) throws BaseException {
        return ResponseUtils.success(categoryService.deleteCategory(id));
    }

}

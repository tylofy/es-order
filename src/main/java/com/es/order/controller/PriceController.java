package com.es.order.controller;

import com.es.order.entity.Category;
import com.es.order.entity.CategoryServiceName;
import com.es.order.exception.BaseException;
import com.es.order.model.CreateCategoryRequest;
import com.es.order.model.CreatePriceRequest;
import com.es.order.model.GetCategoryResponse;
import com.es.order.model.GetPriceResponse;
import com.es.order.service.PriceService;
import com.es.order.util.ResponseData;
import com.es.order.util.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/prise")
public class PriceController {
    @Autowired
    private final PriceService priceService;

    public PriceController(PriceService priceService) {
        this.priceService = priceService;
    }

    @PostMapping
    public ResponseEntity<ResponseData<CategoryServiceName>> createOrUpdatePrise(@RequestBody CreatePriceRequest request) throws BaseException {
        return ResponseUtils.success(priceService.createOrUpdatePrice(request));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseData<GetPriceResponse>> getPrice(@PathVariable(name = "id") String id) throws BaseException {
        return ResponseUtils.success(priceService.getPrice(id));
    }

    @GetMapping
    public ResponseEntity<ResponseData<List<GetPriceResponse>>> getAllPrice() throws BaseException {
        return ResponseUtils.success(priceService.getAllPrice());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseData<String>> deletePrice(@PathVariable(name = "id") String id) throws BaseException {
        return ResponseUtils.success(priceService.deletePrice(id));
    }

}

package com.es.order.controller;

import com.es.order.entity.ServiceName;
import com.es.order.exception.BaseException;
import com.es.order.model.CreateServiceNameRequest;
import com.es.order.model.GetServiceNameResponse;
import com.es.order.service.ServiceNameService;
import com.es.order.util.ResponseData;
import com.es.order.util.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/service-name")
public class ServiceNameController {
    @Autowired
    private final ServiceNameService service;

    public ServiceNameController(ServiceNameService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<ResponseData<ServiceName>> createServiceName(@RequestBody CreateServiceNameRequest request) throws BaseException {
        ServiceName createdServiceName = service.createServiceName(request);
        return ResponseUtils.success(createdServiceName);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseData<GetServiceNameResponse>> getServiceName(@PathVariable(name = "id") String id) throws BaseException {
        return ResponseUtils.success(service.getServiceName(id));
    }

    @GetMapping
    public ResponseEntity<ResponseData<List<GetServiceNameResponse>>> getAllServiceName() throws BaseException {
        return ResponseUtils.success(service.getAllServiceName());
    }

    @PutMapping("/{id}")
    public ResponseEntity<ResponseData<ServiceName>> updateServiceName(@PathVariable(name = "id") String id,
                                                                 @RequestBody CreateServiceNameRequest request) throws BaseException {
        return ResponseUtils.success(service.updateServiceName(id, request));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseData<String>> deleteServiceName(@PathVariable(name = "id") String id) throws BaseException {
        return ResponseUtils.success(service.deleteServiceName(id));
    }

}

# README.md

## Overview

This project Ordering Service use Spring Boot application with a MySQL database, Docker and Docker Compose

## Prerequisites

Ensure the following tools are installed on your system:

- Docker
- Docker Compose

You can verify the installations with the following commands:

```bash
docker --version
docker-compose --version
```

If they're not installed, refer to the official documentation for [Docker](https://docs.docker.com/get-docker/) and [Docker Compose](https://docs.docker.com/compose/install/) to install them.

## Directory Structure

Your project directory should look like this:

```
/es-order
|-- Dockerfile
|-- docker-compose.yml
|-- document
|-- .env
|-- /src
|   |-- (my application's source code)
|-- pom.xml 
```
## Running the Application

To get your application up and running, follow these steps:

1. **Prepare Your Environment**:

    - Navigate to the directory containing `docker-compose.yml`.

    ```bash
    cd es-order
    ```

2. **Start the Application and Database**:

    - Use the following command to launch your Spring Boot application and MySQL database:

    ```bash
    docker-compose up --build
    ```

   The `--build` flag ensures that Docker Compose builds the service images before starting the containers.

3. **Accessing the Application**:

    - Once the services are running, access your Swagger `http://localhost:8085/swagger-ui/index.html#/` in your browser.

3. **Call API**:
    Read document/requestDataExample.txt for get request example and call APIs
4. **Shutting Down**:

    - To stop and remove the containers, networks, and volumes created by Docker Compose, use:

    ```bash
    docker-compose down
    ```


## Conclusion

Following this guide, you can easily set up run application with Docker compose.

